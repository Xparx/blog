---
title: "Morningstar API"
date: 2018-05-13
---

# API overview

Morningstar provides a *secret* api that can be utilized to fetch data for specific stocks, more information can be found here: [gist](https://gist.github.com/hahnicity/45323026693cdde6a116). There is a [python package](https://github.com/petercerno/good-morning) for this that can be used to fetch data.

I want to make an brief analysis of [Telia](http://www.morningstar.com/stocks/XSTO/TELIA/quote.html) by fetching data from [morningstar](http://www.morningstar.com/) to do that.


# Fetching historical data

Lets first setup a generic python environment

    import sys
    import os
    from importlib import reload
    sys.path.insert(0, 'lib/')
    sys.path.insert(0, 'external/')
    import pandas as pd
    import numpy as np
    import scipy.stats as stats
    import re
    pd.set_option('precision',3)
    import matplotlib as mpl
    import matplotlib.pyplot as plt
    import seaborn as sns
    
    color_pallet = "muted"
    plt.style.use('seaborn-ticks')
    sns.set_color_codes(color_pallet)
    # sns.set_context("notebook", font_scale=1.5)
    # sns.set_palette(color_pallet)

Ok, on to the fun stuff. Morningstar provides a ticker search to get the correct ticker. So lets use that.
I wrote some [simple code](https://gitlab.com/Xparx/apihelper) so that I can query the ticker search programmatically.

    import economy_analysis.goodevening as gdv
    lookup = gdv.morningstarLookup()
    tickers = lookup.search("Telia")
    tickers

<table>


<colgroup>
<col  class="org-left">

<col  class="org-left">

<col  class="org-left">

<col  class="org-left">

<col  class="org-left">
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">&#xa0;</th>
<th scope="col" class="org-left">morningstar ticker</th>
<th scope="col" class="org-left">ticker</th>
<th scope="col" class="org-left">Exchange</th>
<th scope="col" class="org-left">name</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">TELIA</td>
<td class="org-left">XSTO:TELIA</td>
<td class="org-left">TELIA</td>
<td class="org-left">XSTO</td>
<td class="org-left">Telia Company AB</td>
</tr>


<tr>
<td class="org-left">TLS</td>
<td class="org-left">XDUS:TLS</td>
<td class="org-left">TLS</td>
<td class="org-left">XDUS</td>
<td class="org-left">Telia Company AB</td>
</tr>


<tr>
<td class="org-left">TELIA1</td>
<td class="org-left">XHEL:TELIA1</td>
<td class="org-left">TELIA1</td>
<td class="org-left">XHEL</td>
<td class="org-left">Telia Company AB</td>
</tr>


<tr>
<td class="org-left">0H6X</td>
<td class="org-left">XLON:0H6X</td>
<td class="org-left">0H6X</td>
<td class="org-left">XLON</td>
<td class="org-left">Telia Company AB</td>
</tr>


<tr>
<td class="org-left">TLSNF</td>
<td class="org-left">TLSNF</td>
<td class="org-left">TLSNF</td>
<td class="org-left">PINX</td>
<td class="org-left">Telia Company AB</td>
</tr>


<tr>
<td class="org-left">TLSNY</td>
<td class="org-left">TLSNY</td>
<td class="org-left">TLSNY</td>
<td class="org-left">PINX</td>
<td class="org-left">Telia Company AB</td>
</tr>


<tr>
<td class="org-left">ZWS</td>
<td class="org-left">XSTU:ZWS</td>
<td class="org-left">ZWS</td>
<td class="org-left">XSTU</td>
<td class="org-left">Telia Lietuva AB</td>
</tr>


<tr>
<td class="org-left">0G8J</td>
<td class="org-left">XLON:0G8J</td>
<td class="org-left">0G8J</td>
<td class="org-left">XLON</td>
<td class="org-left">Telia Lietuva AB</td>
</tr>


<tr>
<td class="org-left">TEL1L</td>
<td class="org-left">XLIT:TEL1L</td>
<td class="org-left">TEL1L</td>
<td class="org-left">XLIT</td>
<td class="org-left">Telia Lietuva AB</td>
</tr>


<tr>
<td class="org-left">TLV</td>
<td class="org-left">XWAR:TLV</td>
<td class="org-left">TLV</td>
<td class="org-left">XWAR</td>
<td class="org-left">Teliani Valley Polska SA</td>
</tr>
</tbody>
</table>

So we can se that there are several different results pointing to the same company. `XSTO` is the stockholm exchange.
Lets try to use that to fetch historical pricing. Note that this functionality is also availibe in the [pandas-datareader](https://pandas-datareader.readthedocs.io) package.

    tkrfetch = gdv.historicalPrice()
    ticker = tickers["morningstar ticker"][whatticker]
    data = tkrfetch.dowload(ticker, currency="SEK")

When using other exchange the data is not the same. For example, using the New york ticker `TLSNY` results in different data. Not sure exactly what is fetched but it does not seem to correspond to the stockholm exchange prices. On top of it being in USD instead of SEK. But I haven't looked closer in to this so it might be correct.

Lets plot an overview of the ticker price and dividend data.
We can already do some interesting observations. Telias dividend seem to correlate with stock price which might be unsurprising. However one striking thing is that the dividend rose several 100% in two years, from 2005 to 2007 as well as recoil back the following two years. The stock prise rose from around 40SEK to around 70SEK in the same time to then fall back to below 40SEK.

![img](/img/XSTO:TELIA/pixel.png)


# Fetching fundamental data

But the real way to evaluate a company is by its fundamentals. Morningstar provides some of these. To fetch them I use the [good-morning](https://github.com/petercerno/good-morning) package. Of note is that morningstar only have data from 2008 onwards for Telia. It's probably the case that only 10 years worth of data is provided by morningstar.

    import good_morning as gm
    KR = gm.KeyRatiosDownloader().download(ticker)
    kr = KR[0].T

One thing to note is that these key ratios also contain dividend data.

    pd.concat([data["dividend"].resample("Y").sum().to_period(),kr['Dividends SEK']],1)

So again, data from good-morning does not contain all data points that the ticker historical data contain. Also comparing some specific years. Most specifically 2018 which have given out 1.15SEK the dividend data from good-morning is less correct. Also looking at years 2008 and 2009 seems to indicate this.

<table>


<colgroup>
<col  class="org-right">

<col  class="org-right">

<col  class="org-right">
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">&#xa0;</th>
<th scope="col" class="org-right">Dividend</th>
<th scope="col" class="org-right">Dividends SEK</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">2001</td>
<td class="org-right">0.5</td>
<td class="org-right">nan</td>
</tr>


<tr>
<td class="org-right">2002</td>
<td class="org-right">0.2</td>
<td class="org-right">nan</td>
</tr>


<tr>
<td class="org-right">2003</td>
<td class="org-right">0.4</td>
<td class="org-right">nan</td>
</tr>


<tr>
<td class="org-right">2004</td>
<td class="org-right">1</td>
<td class="org-right">nan</td>
</tr>


<tr>
<td class="org-right">2005</td>
<td class="org-right">1.2</td>
<td class="org-right">nan</td>
</tr>


<tr>
<td class="org-right">2006</td>
<td class="org-right">3.5</td>
<td class="org-right">nan</td>
</tr>


<tr>
<td class="org-right">2007</td>
<td class="org-right">6.3</td>
<td class="org-right">nan</td>
</tr>


<tr>
<td class="org-right">2008</td>
<td class="org-right">4</td>
<td class="org-right">1.8</td>
</tr>


<tr>
<td class="org-right">2009</td>
<td class="org-right">1.8</td>
<td class="org-right">1.8</td>
</tr>


<tr>
<td class="org-right">2010</td>
<td class="org-right">2.25</td>
<td class="org-right">2.25</td>
</tr>


<tr>
<td class="org-right">2011</td>
<td class="org-right">2.75</td>
<td class="org-right">2.75</td>
</tr>


<tr>
<td class="org-right">2012</td>
<td class="org-right">2.85</td>
<td class="org-right">2.85</td>
</tr>


<tr>
<td class="org-right">2013</td>
<td class="org-right">2.85</td>
<td class="org-right">2.85</td>
</tr>


<tr>
<td class="org-right">2014</td>
<td class="org-right">3</td>
<td class="org-right">3</td>
</tr>


<tr>
<td class="org-right">2015</td>
<td class="org-right">3</td>
<td class="org-right">3</td>
</tr>


<tr>
<td class="org-right">2016</td>
<td class="org-right">3</td>
<td class="org-right">3</td>
</tr>


<tr>
<td class="org-right">2017</td>
<td class="org-right">2</td>
<td class="org-right">2</td>
</tr>


<tr>
<td class="org-right">2018</td>
<td class="org-right">1.15</td>
<td class="org-right">1</td>
</tr>
</tbody>
</table>


# Calculating key ratios

Lets look at some percentage key values while we are at it.
The most striking thing is the payout ratio which, disregarding the high variation the last couple of years is almost all of the profits made by Telia. While the dividend in general has decreased if we look at previous figures.

![img](/img/XSTO:TELIAkeyRatiosPercent/pixel.png)

Lets look at some of the Key Ratios retrieved with `good-morning` and calculate some interesting key values over time. `P/E` is defined as \(\frac{Stock Price}{Earnings per share}\) and are often used to estimate if a stock is expensive or not, a few companions to `P/E` is `P/B` value, \(\frac{Stock Price}{Book value per share}\) as well as `P/S` Price over revenue per share (in this case).

    PE = (data["price"]["Open"] / kr["Earnings Per Share SEK"].resample("D").pad()).dropna()
    PB = (data["price"]["Open"] / kr["Book Value Per Share * SEK"].resample("D").pad()).dropna()
    PS = ((kr["Shares Mil"] / kr["Revenue SEK Mil"]).resample("D").pad() * data["price"]["Open"]).dropna()

    with mpl.rc_context(rc={'font.family': ['serif']}):
        sns.set_context("notebook", font_scale=1.5)
        fig, ax = plt.subplots(nrows=3, ncols=1, facecolor='w', edgecolor='k', figsize=(10,10), dpi=75, sharex=True)
        __ = PE.plot(ax=ax[0])
        __ = ax[0].set_ylabel("P/E",  color='gray', rotation='horizontal')
        __ = ax[0].yaxis.set_label_coords(0.01, 1.01)
    
        __ = PB.plot(ax=ax[1])
        __ = ax[1].set_ylabel("P/B",  color='gray', rotation='horizontal')
        __ = ax[1].yaxis.set_label_coords(0.01, 1.01)
    
        __ = PS.plot(ax=ax[2])
        __ = ax[2].set_ylabel("P/S",  color='gray', rotation='horizontal')
        __ = ax[2].yaxis.set_label_coords(0.01, 1.01)
    
        __ = sns.despine(offset=0, trim=False)
        for x in ax:
            __ = x.minorticks_on()
            __ = x.grid(which="major", color='darkgray', linestyle='--', linewidth=1)
            __ = x.grid(which="minor", color='lightgray', linestyle='--', linewidth=0.5)
            __ = x.tick_params(which='minor', # This needs to be after despine or it does not stick
                               top=False, # turn off top ticks
                               left=False, # turn off left ticks
                               right=False,  # turn off right ticks
                               bottom=False) # turn off bottom ticks
            __ = x.set_axisbelow(True) # This can be put after minorticks_on line.
        __ = fig.tight_layout(pad=0.0)
    
    fname = "/img/" + ticker + "keyRatios"
    
    if not os.path.exists(fname):
        os.makedirs(fname, exist_ok=True)
    
    fig.savefig(os.path.join(fname, "pixel.png"), transparent=False)
    fig.savefig(os.path.join(fname, "vector.svg"), transparent=False)
    
    print(fname+"/pixel.png", sep=",", end="")

One thing to note, looking at key ratios, is that the `P/E` ratio has sky rocketed the last couple of years with some periodicity baked in.

![img](/img/XSTO:TELIAkeyRatios/pixel.png)


# Financial data

It's also possible to fetch financial data for the specified ticker. Further analysis is left for a later time.

    FINS = gm.FinancialsDownloader().download(ticker)
    pd.DataFrame([i for i in FINS.keys()], columns=["content"])

<table>


<colgroup>
<col  class="org-right">

<col  class="org-left">
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">&#xa0;</th>
<th scope="col" class="org-left">content</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">0</td>
<td class="org-left">income_statement</td>
</tr>


<tr>
<td class="org-right">1</td>
<td class="org-left">currency</td>
</tr>


<tr>
<td class="org-right">2</td>
<td class="org-left">period_range</td>
</tr>


<tr>
<td class="org-right">3</td>
<td class="org-left">cash_flow</td>
</tr>


<tr>
<td class="org-right">4</td>
<td class="org-left">fiscal_year_end</td>
</tr>


<tr>
<td class="org-right">5</td>
<td class="org-left">balance_sheet</td>
</tr>
</tbody>
</table>


# Conculsion

It's fairly easy to fetch both historical stock price and fundamentals about a specific ticker. With the added code it's also possible to search for what ticker to use for a specific company. For Telia I use the ticker for the stockholm stock exchange but Telia is also traided in Finland with the ticker `TELIA1` which can also be found in the search results.

To calculate what investing in Telia in 2000-06-13 would have given us today, not counting dividend we can simply check the relative price day to day and use the cumulative product.

    deltaVal = (data["price"]/ data["price"].shift())
    print("{:.2f}%".format(deltaVal["Close"].cumprod().iloc[-1]*100))

46.84%

Investing in Telia in 2000 would have halved our investment today. This is of course easy to se from the historical price graph.

